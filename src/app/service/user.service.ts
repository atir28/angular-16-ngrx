import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from 'src/Store/Model/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  baseurl = 'http://localhost:3000/user';
  constructor(private _http: HttpClient) {}

  GetAll() {
    return this._http.get<User[]>(this.baseurl);
  }

  Getbycode(code: number) {
    return this._http.get<User>(this.baseurl + '/' + code);
  }
  Delete(code: number) {
    return this._http.delete(this.baseurl + '/' + code);
  }
  Update(data: User) {
    return this._http.put(this.baseurl + '/' + data.id, data);
  }
  Create(data: User) {
    return this._http.post(this.baseurl, data);
  }
}
