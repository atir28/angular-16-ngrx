import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateUserComponent } from '../create-user/create-user.component';
import { Store } from '@ngrx/store';
import { User } from 'src/Store/Model/user.model';
import { geUserList } from 'src/Store/user/user.selectors';
import { loadUser } from 'src/Store/user/user.acton';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  userList!: User[];
  dataSource: any;
  displayedColums: string[] = [
    'code',
    'name',
    'email',
    'phone',
    'address',
    'type',
    'group',
    'status',
    'action',
  ];

  @ViewChild(MatPaginator) _paginator!: MatPaginator;
  @ViewChild(MatSort) _matSort!: MatSort;

  constructor(private _dialog: MatDialog, private _store: Store) {}

  ngOnInit(): void {
    this._store.dispatch(loadUser());

    this._store.select(geUserList)?.subscribe((userList: any) => {
      this.userList = userList;
      console.log(this.userList);

      this.dataSource = new MatTableDataSource<User>(this.userList);
      this.dataSource.sort = this._matSort; 
      this.dataSource.paginator = this._paginator
    });
  }

  
  openPopup(code: number, title: string): void {
    this._dialog.open(CreateUserComponent, {
      width: '50%',
      disableClose: true,
      enterAnimationDuration: '1000ms',
      exitAnimationDuration: '1000ms',
      data: {
        code: code,
        title: title,
      },
    });
  }
  addUser() {
    this.openPopup(0, 'Create User');
  }

  editUser(code: number): void { }
  
  deleteUser(code: number) {
     if (confirm('do you want to remove?')) {
      //  this.store.dispatch(deleteeassociate({ code: code }));
     }
  }
}
