import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss'],
})
export class CreateUserComponent implements OnInit {
  title = 'Create User';
  isEditable = false;
  dialogData: any;
  constructor(
    private _fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
  ngOnInit(): void {
    this.dialogData = this.data;
    this.title = this.dialogData.title
  }

  userForm = this._fb.group({
    id: this._fb.control(0),
    name: this._fb.control('', Validators.required),
    email: this._fb.control(
      '',
      Validators.compose([Validators.required, Validators.email])
    ),
    phone: this._fb.control('', Validators.required),
    address: this._fb.control('', Validators.required),
    type: this._fb.control('CUSTOMER'),
    group: this._fb.control('level1'),
    status: this._fb.control(true),
  });

  saveUserForm() {
    if (this.userForm.valid) {
    }
  }
}
