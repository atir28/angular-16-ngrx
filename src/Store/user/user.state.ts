import { UserModel } from "../Model/user.model";

export const UserState: UserModel = {
  list: [],
  errorMessage: '',
  userObj: {
    id: 0,
    name: '',
    email: '',
    phone: '',
    type: 'CUSTOMER',
    address: '',
    associategroup: 'level1',
    status: true,
  },
};