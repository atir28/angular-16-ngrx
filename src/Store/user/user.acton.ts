import { createAction, props } from '@ngrx/store';
import { User } from '../Model/user.model';

export const LOAD_USER = '[user page] load user';
export const LOAD_USER_SUCCESS = '[user page] load user success';
export const LOAD_USER_FAIL = '[user page] load user fail';

export const loadUser = createAction(LOAD_USER);
export const loadUserSuccess = createAction(
  LOAD_USER_SUCCESS,
  props<{ list: User[] }>()
);
export const loadUserFail = createAction(
  LOAD_USER_FAIL,
  props<{ errorMessage: string }>()
);
