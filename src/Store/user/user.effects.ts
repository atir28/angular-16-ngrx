import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { loadUser, loadUserFail, loadUserSuccess } from './user.acton';
import { catchError, exhaustMap, map, of } from 'rxjs';
import { UserService } from 'src/app/service/user.service';

@Injectable()
export class UserEffects {
  constructor(private action$: Actions, private _userService: UserService) {}

  _loadUser = createEffect(() =>
    this.action$.pipe(
      ofType(loadUser),
      exhaustMap((action) => {
        return this._userService.GetAll().pipe(
          map((data) => {
            return loadUserSuccess({ list: data });
          }),
          catchError((_error) =>
            of(loadUserFail({ errorMessage: _error.message }))
          )
        );
      })
    )
  );
}
