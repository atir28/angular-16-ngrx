import { createFeatureSelector, createSelector } from "@ngrx/store";
import { UserModel } from "../Model/user.model";


const getUserState = createFeatureSelector<UserModel>('user');

export const geUserList = createSelector(getUserState, (state) => {
    return state?.list;
})