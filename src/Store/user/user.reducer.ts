import { createReducer, on } from '@ngrx/store';
import { UserState } from './user.state';
import { loadUserFail, loadUserSuccess } from './user.acton';

const _UserReducer = createReducer(
  UserState,
  on(loadUserSuccess, (state, action) => {
    return {
      ...state,
      list: [...action.list],
      errorMessage: '',
    };
  }),
  on(loadUserFail, (state, action) => {
    return {
      ...state,
      list: [],
      errorMessage: action.errorMessage,
    };
  })
);

export function UserReducer(state: any, action: any) {
    return _UserReducer(state, action);
}
